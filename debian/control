Source: mktorrent
Section: net
Priority: optional
Maintainer: Paride Legovini <pl@ninthfloor.org>
Build-Depends: debhelper (>= 11~), libssl-dev
Standards-Version: 4.2.0
Homepage: https://github.com/Rudde/mktorrent
Vcs-Git: https://salsa.debian.org/debian/mktorrent.git
Vcs-Browser: https://salsa.debian.org/debian/mktorrent

Package: mktorrent
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: foreign
Description: simple command line utility to create BitTorrent metainfo files
 mktorrent is a text-based utility to create BitTorrent metainfo files
 used by trackers and torrent clients. It can create metainfo files for
 single files or complete directories in a fast way.
 .
 It supports:
  - multiple trackers,
  - embedding custom comments into torrent files,
  - multi-threaded hashing.
 .
 It also supports setting the "private" flag, which advises the BitTorrent
 agents to refrain from using alternative peer discovery mechanisms,
 such as Distributed Hash Table (DHT), Local Peer Discovery (LPD),
 or Peer Exchange.
